package dronecw;

public class Drone {
	public static int drone_identifier = 0;
	private Integer x_pos;
	private Integer y_pos;
	private Integer specific_drone;
	private Direction current_direction;
	
	//drone constructor
	Drone(int x, int y, Direction dir){
		y_pos = Integer.valueOf(y);
		x_pos = Integer.valueOf(x);
		current_direction = dir;
		specific_drone = drone_identifier;
		drone_identifier++;
	}
	
	//check if drone is in a position
	public boolean isHere(int x, int y) {
		if(x_pos == x && y_pos == y) {
			return true;
		} else {
			return false;
		}
	}
	
	//try to move the drone to a new position
	public void tryToMove(DroneArena a) {
		switch(current_direction) {
		case North:
			if(a.canMoveHere(x_pos, y_pos-1)) {
				a.moveDrone(this, x_pos, y_pos-1);
				y_pos--;
			}
			else {
				current_direction = current_direction.getNextDir();
			}
			break;
			
		case East:
			if(a.canMoveHere(x_pos+1, y_pos)) {
				a.moveDrone(this, x_pos+1, y_pos);
				x_pos++;
			}
			else {
				current_direction = current_direction.getNextDir();
			}
			break;
		case South:
			if(a.canMoveHere(x_pos, y_pos+1)) {
				a.moveDrone(this, x_pos, y_pos+1);
				y_pos++;
			}
			else {
				current_direction = current_direction.getNextDir();
			}
			break;
		case West:
			if(a.canMoveHere(x_pos-1, y_pos)) {
				a.moveDrone(this, x_pos-1, y_pos);
				x_pos--;
			}
			else {
				current_direction = current_direction.getNextDir();
			}
			break;
		}
	}
	
	//shows drone on canvas
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(x_pos,y_pos,'D');
	}
	
	//getters
	
	public int getXPos() {
		return x_pos;
	}
	public int getYPos() {
		return y_pos;
	}
	public int getCurrentDrone() {
		return specific_drone;
	}
	public Direction getDirection() {
		return current_direction;
	}
	public void setSpecificDrone(int current_drone) {
		specific_drone = current_drone;
	}
}
