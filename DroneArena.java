package dronecw;

import java.util.Random;

public class DroneArena {

	//setters
	private Integer x_size;
	private Integer y_size;
	private Drone[][] arena;
	private Random randomGenerator;
	public Drone[] drone_list;
	public static Integer number_of_drones = 0;
	private Drone posDrone;
	
	//constructor for dronearena
	DroneArena(int x, int y){
		y_size = Integer.valueOf(y);
		x_size = Integer.valueOf(x);
		arena = new Drone[x][y];
		randomGenerator = new Random();
		drone_list = new Drone[x*y];
		
	}
	
	public void addPosDrone(int current_drone, int x_pos, int y_pos, String str) {
		
		
		//setting the direction
		if(str.equals("North"))
		{
			
			 posDrone = new Drone(x_pos,y_pos,Direction.North);
		}
		if(str.equals("East"))
		{
			 
			 posDrone = new Drone(x_pos,y_pos,Direction.East);
		}
		if(str.equals("South"))
		{
		    
			 posDrone = new Drone(x_pos,y_pos,Direction.South);
		}
		if(str.equals("West"))
		{
			 
			 posDrone = new Drone(x_pos,y_pos,Direction.West);

		}
		

		posDrone.setSpecificDrone(current_drone);
		int number_of_drones = 0;
		for(Drone d : drone_list) {
			if(d != null) {
				number_of_drones++;
			}
		}
		
		
		//adds the drone to the end of drone list
		drone_list[number_of_drones+1] = posDrone;

	}
	//adds a drone
	public void addDrone() {
		Integer random_y = null;
		Integer random_x = null;
		do {
			random_y = randomGenerator.nextInt(y_size);
			random_x = randomGenerator.nextInt(x_size);
		} while(getDroneAt(random_x, random_y) instanceof Drone || random_y <= 1 || random_x <= 1);
		
		//add a new drone at random position
		Drone d = new Drone(random_x, random_y, Direction.getRandomDir());
		
		//add new drone to drone list
		drone_list[number_of_drones]= d;
		number_of_drones++;
		
		//add the drone into the arena
		arena[random_x][random_y] = d;
	}	
	
	//getters
	public Drone[] getDroneList() {
		return drone_list;
	}
	
	public Drone getDroneAt(int x, int y) {
		if (arena[x][y] instanceof Drone) {
			return arena[x][y];
		} else {
			return null;
		}
	}
	
	
	//displays drone on canvas
	public void showDrones(ConsoleCanvas c) {
		for(Drone d : drone_list){
			if(d != null) {
				d.displayDrone(c);
			}
		}
	}
	
	//getters
	public Integer getX() {
		return x_size;
	}
	
	public Integer getY() {
		return y_size;
	}
	
	//checking if in bounds
	public boolean canMoveHere(Integer x_pos, Integer y_pos) {
		if(x_pos >= x_size-1 || x_pos<=0) {
			return false;	
		}
		else if(y_pos >= y_size || y_pos<=0){
			return false;
		}
		else if(getDroneAt(x_pos, y_pos) instanceof Drone) {
			return false;
		}
		else {
			return true;
		}
	}
	
	//moves all drones
	public void moveAllDrones() {
		for(Drone d : drone_list) {
			if(d != null) {
				d.tryToMove(this);
			}
		}
	}
	
	//moves a drone
	public void moveDrone(Drone d, Integer new_x, Integer new_y) {
		arena[d.getXPos()][d.getYPos()] = null;
		arena[new_x][new_y]=d;
	}
}

