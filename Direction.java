package dronecw;

import java.util.Random;

public enum Direction {
    North,
    East,
    South,
    West;

    public static Direction getRandomDir() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
    
    public Direction getNextDir() {
    	return values()[(this.ordinal()+1) % values().length];
    }
}
