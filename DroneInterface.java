package dronecw;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;



public class DroneInterface {
	
		//setters
		private Scanner s;							
	   	private DroneArena myArena;	
	   	String str;
	   	
	   	//constructor
	    public DroneInterface() {
	    	s = new Scanner(System.in);			
	    	myArena = new DroneArena(20, 6);
	        char choice = ' ';
	        do {
	        	//menu choices
	        	System.out.print("Enter A: add drone, D: display drones, M: move drones, N: animate the drones, B: create new arena, X: exit > ");
	        	choice = s.next().charAt(0);
	        	s.nextLine();
	        	switch (choice) {
	    			case 'A' :
	    			case 'a' :
	        			myArena.addDrone();
	        			break;
	        		case 'D' :
	        		case 'd' :
	        			this.doDisplay();
	        			break;
	        		case 'M':
	        		case 'm':
	        			myArena.moveAllDrones();
	        			this.doDisplay();
	        			break;
	        		case 'N':
	        		case 'n':
	        			this.animateDrones();
	        			break;
	        		case 'B':
	        		case 'b':
	        			System.out.print("Enter the new x size >");
	        			int x_size = s.nextInt();
	        			System.out.print("Enter the new y size >");
	        			int y_size = s.nextInt();
	        			myArena = new DroneArena(x_size, y_size);
	        			break;

	        		case 'x' : 	choice = 'X';				
	        			break;
	        		
	        	}
	    		} while (choice != 'X');						
	       s.close();									
	    }
	    
	    //Animate the drones
	    private void animateDrones() {
	    	int count = 10;
	    	while(count>0) {
				
				myArena.moveAllDrones();
    			this.doDisplay();
    			count--;
    			try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
	    }
	    

	    //displays the canvas
	    public void doDisplay() {
	    	Integer y_size = myArena.getY();
	    	Integer x_size = myArena.getX();
	    	ConsoleCanvas c = new ConsoleCanvas(x_size,y_size);
	    	myArena.showDrones(c);
	    	System.out.print(c.writeCanvas());
	    }
	    
		//Main function for the package
		public static void main(String[] args) {
			DroneInterface r = new DroneInterface();
		}

}
